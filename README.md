# Markdown Presentation Template

A markdown presentation template for future use, powered with Marp.

## Installation

0. Requires node package manager and its ecosystems.
1. Run `npm install` in the root of the project. Wait until it finishes.
2. Run `npm start PRESENTATION.md` to present.

## Usage

The user should know at least a bit about creating markdown and should read more about how to create markdown presentation in https://github.com/jxnblk/mdx-deck.

## Presenting
1. Enter presenter mode by pressing Opt + P
2. Click the link at the bottom to open the presentation in another tab
3. Move the new tab to its own window in the screen or projector that the audience sees
4. Control the presentation from the original window
5. Be sure to hide your mouse cursor so that it's not visible to the audience


## Authors and acknowledgment
Tengku Izdihar Rahman Amanullah and the fine folks at Marp.

## License
It's licensed under AGPLv3. Because why not.